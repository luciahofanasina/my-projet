import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_hud/progress_hud.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:projet/CoverPassword/coverpasswordView.dart';
import 'package:projet/Home/homeView.dart';
import 'package:projet/Inscription/inscriptionView.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _passwordVisible = false;
  bool show = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () async => false,
      child: new MaterialApp(
        theme: ThemeData(
          primaryColor: Colors.white,
        ),
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height * 1,
              child: Stack(children: <Widget>[
                Container(
                  child: LayoutBuilder(
                    builder: (BuildContext context,
                        BoxConstraints viewportConstraints) {
                      return ConstrainedBox(
                        constraints: BoxConstraints(
                            minHeight: viewportConstraints.maxHeight),
                        child: Container(
                          child: IntrinsicHeight(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Card(
                                    child: Image.asset(
                                      "assets/icam/camson.JPG",
                                      fit: BoxFit.fitWidth,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.1,
                                    ),
                                  ),
                                ),
                                Card(
                                  elevation: 0,
                                  //ito le amban  color: Colors.purpleAccent,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(0.0)),
                                  color: Colors.pinkAccent[100],
                                  child: ListTile(
                                    title: Text(
                                      "Gestion Parc Informatique",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12.0,
                                      ),
                                    ),
                                    //leading: Image.asset('assets/app_sed.jpg'),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                SingleChildScrollView(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.1,
                                        right:
                                            MediaQuery.of(context).size.width *
                                                0.12),
                                    padding: EdgeInsets.only(
                                        top: 10.0, left: 20.0, right: 10.0),
                                    decoration: BoxDecoration(
                                      color: Colors.transparent,
                                      borderRadius: BorderRadius.circular(50.0),
                                    ),
                                    child: Column(
                                      children: <Widget>[
                                        FocusScope(
                                            child: Focus(
                                          onFocusChange: (focus) =>
                                              print("focus: $focus"),
                                          child: TextField(
                                            // controller: _matricule,
                                            autocorrect: true,
                                            decoration: InputDecoration(
                                              contentPadding: EdgeInsets.all(8),
                                              fillColor: Colors.grey[300],
                                              filled: true,
                                              hintText: "Nom d'utilisateur",
                                              hintStyle: TextStyle(
                                                color: Colors.black,
                                              ),
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                borderSide: BorderSide(
                                                  color:
                                                      Colors.deepPurpleAccent,
                                                  width: 3,
                                                ),
                                              ),
                                              disabledBorder:
                                                  OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey[300]),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey[300]),
                                              ),
                                            ),
                                          ),
                                        )),
                                        SizedBox(height: 20.0),
                                        Container(
                                          height: 75.0,
                                          child: Stack(
                                            alignment: const Alignment(0, 0),
                                            children: <Widget>[
                                              Container(
                                                  child: Padding(
                                                padding: EdgeInsets.only(
                                                    left: 0, right: 0, top: 5),
                                                child: FocusScope(
                                                    child: Focus(
                                                  onFocusChange: (focus) =>
                                                      print("focus: $focus"),
                                                  child: TextFormField(
                                                    validator: (value) {
                                                      if (value.isEmpty) {
                                                        return 'entrer mot de passe';
                                                      }
                                                      return null;
                                                    },
                                                    autocorrect: true,
                                                    // controller: _password,
                                                    obscureText:
                                                        !_passwordVisible,
                                                    decoration: InputDecoration(
                                                      contentPadding:
                                                          EdgeInsets.all(8),
                                                      fillColor:
                                                          Colors.grey[300],
                                                      filled: true,
                                                      hintText: 'Mot de passe',
                                                      hintStyle: TextStyle(
                                                        color: Colors.black,
                                                      ),
                                                      disabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                        borderSide: BorderSide(
                                                            width: 1,
                                                            color: Colors
                                                                .deepPurpleAccent),
                                                      ),
                                                      enabledBorder:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                        borderSide: BorderSide(
                                                            width: 1,
                                                            color: Colors
                                                                .grey[300]),
                                                      ),
                                                      border:
                                                          OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                        borderSide: BorderSide(
                                                          color: Colors.black,
                                                          width: 3,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                )),
                                              )),
                                              Positioned(
                                                right: 15,
                                                child: IconButton(
                                                  icon: Icon(
                                                    // Based on passwordVisible state choose the icon
                                                    _passwordVisible
                                                        ? Icons.visibility
                                                        : Icons.visibility_off,
                                                    color: Colors.black,
                                                  ),
                                                  onPressed: () {
                                                    // Update the state i.e. toogle the state of passwordVisible variable
                                                    setState(() {
                                                      _passwordVisible =
                                                          !_passwordVisible;
                                                    });
                                                  },
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 40.0,
                                  margin: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.05,
                                      left: MediaQuery.of(context).size.width *
                                          0.12,
                                      right: MediaQuery.of(context).size.width *
                                          0.12),
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white,
                                  ),
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    )),
                                    child: show == false
                                        ? Text(
                                            'SE CONNECTER',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 22.0),
                                          )
                                        : Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              CircularProgressIndicator(
                                                color: Colors.white,
                                              ),
                                              const SizedBox(width: 12),
                                              Text("Veuillez attendre...",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 22.0))
                                            ],
                                          ),
                                    color: Colors.purpleAccent,
                                    splashColor: Colors.blue,
                                    onPressed: () {
                                      setState(() {
                                        show = true;
                                      });
                                      // loginMaep();
                                       Navigator.of(context)
                                                    .push(MaterialPageRoute(
                                                  builder: (context) =>
                                                      Home(),
                                                ));},
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.width *
                                          0.05),
                                  child: Center(
                                    child: RichText(
                                      text: TextSpan(children: [
                                        TextSpan(
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                Navigator.of(context)
                                                    .push(MaterialPageRoute(
                                                  builder: (context) =>
                                                      NewAccount(),
                                                ));
                                              },
                                            text: "Créer nouveau compte",
                                            style: TextStyle(
                                              fontFamily: 'SFUIDisplay',
                                              color: Colors.purpleAccent,
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.03,
                                            )),
                                        TextSpan(text: '       '),
                                        TextSpan(
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                Navigator.of(context)
                                                    .push(MaterialPageRoute(
                                                  builder: (context) =>
                                                      CoverPassword(),
                                                ));
                                              },
                                            text: "Mot de passe oublier ",
                                            style: TextStyle(
                                              fontFamily: 'SFUIDisplay',
                                              color: Colors.grey,
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.03,
                                            )),
                                      ]),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                )
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Container _buildDivider() {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      width: double.infinity,
      height: 1.0,
      color: Colors.grey.shade400,
    );
  }
}
