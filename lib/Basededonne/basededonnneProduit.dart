import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBproduit {
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "produit.db");
    var theDb = await openDatabase(path,
        version: 1, onCreate: _onCreate, onConfigure: _onConfigure);
    return theDb;
  }

  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }

  void _onCreate(Database db, int version) async {
    await db.transaction((txn) async {
      await txn.execute(
          "CREATE TABLE produit(id_produit INTEGER PRIMARY KEY AUTOINCREMENT,  num_serie VARCHAR(255)  NOT NULL,  num_Qrcode VARCHAR(255)  NOT NULL,  designation VARCHAR(255)  NOT NULL,  marque VARCHAR(255)  NOT NULL,  modele VARCHAR(255)  NOT NULL,  produitcode VARCHAR(255)  NOT NULL,  longueur VARCHAR(255)  NOT NULL,  largeur VARCHAR(255)  NOT NULL,  hauteur VARCHAR(255)  NOT NULL,  poids VARCHAR(255)  NOT NULL,  produit_dstatus VARCHAR(255)  NOT NULL,  emplacement VARCHAR(255)  NOT NULL,  date_entree VARCHAR(255)  NOT NULL,  date_sortie VARCHAR(255)  NOT NULL,  info_sortie VARCHAR(255)  NOT NULL,  etat VARCHAR(255)  NOT NULL,  description VARCHAR(255)  NOT NULL        );");
    
    });
  }

Future<List> inventaire() async {
    Database db = await this.db;

    List<Map> image = await db
        .rawQuery("SELECT COUNT(*) as quantite,designation,date_entree,date_sortie FROM produit GROUP BY produitcode ;");
    print("get image $image");
    return image;
  }
   Future<List> listestock() async {
    Database db = await this.db;

    List<Map> image = await db
        .rawQuery("SELECT  designation,num_serie,num_Qrcode FROM produit GROUP BY produitcode ;");
    print("get image $image");
    return image;
  }

  Future<List> liste( ) async {
    Database db = await this.db;

    List<Map> image = await db
        .rawQuery("SELECT  * FROM produit ");
    print("get image $image");
    return image;
  }

  Future<List> seule(String id) async {
    Database db = await this.db;

    List<Map> image =
        await db.rawQuery("SELECT  COUNT(*) as quantite,marque,modele,designation FROM produit WHERE num_Qrcode='$id' GROUP BY num_Qrcode");

    return image;
  }

  Future<int> inserer(String num_serie , String num_Qrcode , String designation , String marque , String modele , String produitcode , String longueur , String largeur , String hauteur ,  String poids ,  String produit_dstatus , String emplacement , String  date_entree ,String  date_sortie , String info_sortie , String etat , String description  )async {
  
    Database db = await this.db;
    int id = await db.rawInsert(
        'INSERT INTO produit (num_serie ,  num_Qrcode ,  designation ,  marque ,  modele ,  produitcode ,  longueur ,  largeur ,  hauteur ,  poids ,  produit_dstatus ,  emplacement ,  date_entree ,  date_sortie ,  info_sortie ,  etat ,  description  ) VALUES("$num_serie" ,  "$num_Qrcode" ,  "$designation" ,  "$marque" ,  "$modele" ,  "$produitcode" ,  "$longueur" ,  "$largeur" ,  "$hauteur" ,  "$poids" ,  "$produit_dstatus" ,  "$emplacement" ,  "$date_entree" ,  "$date_sortie" ,  "$info_sortie" ,  "$etat" ,  "$description"  );  ');
    return id;
  }
}
