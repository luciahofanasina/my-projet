import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBIMAGE {
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "Image.db");
    var theDb = await openDatabase(path,
        version: 1, onCreate: _onCreate, onConfigure: _onConfigure);
    return theDb;
  }

  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }

  void _onCreate(Database db, int version) async {
    await db.transaction((txn) async {
      await txn.execute(
          "CREATE TABLE PHOTO(id_image INTEGER PRIMARY KEY AUTOINCREMENT,photo_name TEXT,numQrcode VARCHAR(255));");
    });

    print("Created tables PHOTO");
  }

  //get enquete
  Future<List> getImage(String id) async {
    Database db = await this.db;

    List<Map> image =
        await db.rawQuery("SELECT  photo_name FROM PHOTO WHERE numQrcode='$id'");
  print("get image $image");
    return image;
  }

  Future<int> insertImage(String photo_name,String numQrcode) async {
    print("tafiditra Function insert ");
    Database db = await this.db;
    int id = await db
        .rawInsert('INSERT INTO  PHOTO (photo_name,numQrcode) VALUES("$photo_name","$numQrcode");  ');
    return id;
  }
}
