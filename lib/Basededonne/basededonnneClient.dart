import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class Client {
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "Client.db");
    var theDb = await openDatabase(path,
        version: 1, onCreate: creation, onConfigure: cofigurer);
    return theDb;
  }

  static Future cofigurer(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }

  void creation(Database db, int version) async {
    await db.transaction((txn) async {
      await txn.execute(
          "CREATE TABLE client(id_cli INTEGER PRIMARY KEY AUTOINCREMENT,societe  VARCHAR(255),num_contrat VARCHAR(255),date VARCHAR(255));");
    });
  }

  Future<List> listeClient() async {
    Database db = await this.db;

    List<Map> image = await db.rawQuery("SELECT  * FROM client");
    print("get image $image");
    return image;
  }

  Future<List> one(int id) async {
    Database db = await this.db;

    List<Map> image =
        await db.rawQuery("SELECT  * FROM client WHERE id_cli='$id'");
    print("get image $image");
    return image;
  }

  Future<int> insert(String societe, String num_contrat, String date) async {
    Database db = await this.db;
    int id = await db.rawInsert(
        'INSERT INTO client (societe,num_contrat,date ) VALUES("$societe","$num_contrat","$date" );  ');
    return id;
  }
}
