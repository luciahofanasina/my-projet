import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_hud/progress_hud.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:projet/Basededonne/basededonnneProduit.dart';
import 'package:projet/Inventaire/inventaireView.dart';
import 'package:projet/Login/loginView.dart';
import 'package:projet/Produit/listeProduit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';

class ListStock extends StatefulWidget {
  @override
  _ListStockState createState() => _ListStockState();
}

List stock = List();

class _ListStockState extends State<ListStock> {
  @override
  void initState() {
    super.initState();
    liste();
  }

  liste() async {
    var inserer = DBproduit();
    Future<List> future = inserer.listestock();
    List stock1 = await future;
    setState(() {
      stock = stock1;
    });
  }

  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async => true,
        child: new Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(
                color: Colors.black,
                size: MediaQuery.of(context).size.height * 0.05),
            elevation: 0,
            backgroundColor: Colors.white,
          ),
          body:SingleChildScrollView(
        reverse: true,
        child: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.85,
                margin: EdgeInsets.only(top: 12),
                child: new Column(children: [
                  Container(
                    margin: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.01,
                        top: MediaQuery.of(context).size.height * 0),
                    child: Card(
                      child: Image.asset("assets/icam/camson.JPG",
                          height: MediaQuery.of(context).size.height * 0.1,
                          fit: BoxFit.fitWidth),
                    ),
                  ),
                  Card(
                    elevation: 0,
                    //ito le amban  color: Colors.purpleAccent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0)),
                    color: Colors.pinkAccent[100],
                    child: ListTile(
                      title: Text(
                        "Liste des produits en Stock",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12.0,
                        ),
                      ),
                      trailing: Icon(Icons.person),
                    ),
                  ),
                  Container(
                      height: MediaQuery.of(context).size.height * 0.08,
                      child: Row(children: [
                        Expanded(
                            child: SizedBox(
                              width: 10,
                          child: FocusScope(
                              child: Focus(
                            onFocusChange: (focus) => print("focus: $focus"),
                            child: TextField(
                              // controller: ,
                              autocorrect: true,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(8),
                                fillColor: Colors.grey[300],
                                filled: true,
                                hintText: "N° DE",
                                hintStyle: TextStyle(
                                  color: Colors.black,
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Colors.deepPurpleAccent,
                                    width: 3,
                                  ),
                                ),
                                disabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      width: 1, color: Colors.grey[300]),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      width: 1, color: Colors.grey[300]),
                                ),
                              ),
                            ),
                          )),
                        )),
                        const SizedBox(width: 16.0),

                        /*  Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: DropdownButton(
                                          underline:
                                                              Container(),
                                          items: produitStatusList
                                              .map((value) =>
                                                  new DropdownMenuItem(
                                                    child: new Text(
                                                      value,
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 15,
                                                      ),
                                                    ),
                                                    value: value,
                                                  ))
                                              .toList(),
                                          onChanged: (secteurType) {
                                            setState(() {
                                              status=
                                                  secteurType;

                                              FocusScope.of(context)
                                                  .requestFocus(
                                                      new FocusNode());
                                            });
                                          },
                                          value: status ,
                                          dropdownColor:
                                            Colors.grey[300],
                                          iconEnabledColor: Colors.red,
                                          iconDisabledColor: Colors.black,
                                          isExpanded: true,
                                          hint: Text(
                                            '                        ',
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ),
                                      )),
                                    ),  */
                      ])),
                  
                  Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.01),
                      height: MediaQuery.of(context).size.height * 0.1,
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(child: Text("")),
                            const SizedBox(width: 16.0),
                            Expanded(
                              child: InkWell(
                                onTap: () async {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => ListeInventaire(),
                                  ));
                                },
                                child: Card(
                                  color: Colors.green,
                                  elevation: 4.0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Faire un inventaire',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ])),
                  SizedBox(height: MediaQuery.of(context).size.width * 0.01),
                  Container(
                    child: PaginatedDataTable(
                      rowsPerPage: 4,
                      columns: [
                        DataColumn(label: Text('Nom du Produit')),
                        DataColumn(label: Text('Quantité')),
                        DataColumn(label: Text('Stock Entée')),
                        DataColumn(label: Text('Stock Sortie')),
                      ],
                      source: _DataSource(context),
                    ),
                  ),
                ]),
              ),
            ],
          ),),
        ));
  }
}

class _Row {
  _Row(
    this.valueA,
    this.valueB,
    this.valueC,
    this.valueD,
  );

  final String valueA;
  final String valueB;
  final String valueC;
  final String valueD;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  _DataSource(this.context) {
    _rows = <_Row>[
      if (stock.length != 0)
        for (int i = 0; i <= stock.length; i++)
          _Row('${stock[0]["designation"]}', '${stock[0]["quantite"]}',
              '${stock[0]["date_entree"]}', '${stock[0]["date_sortie"]}'),
    ];
  }

  final BuildContext context;
  List<_Row> _rows;

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow(
      cells: [
        DataCell(Text(row.valueA)),
        DataCell(Text(row.valueB)),
        DataCell(Text(row.valueC)),
        DataCell(Text(row.valueD)),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
