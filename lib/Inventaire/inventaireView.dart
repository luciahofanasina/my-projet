import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_hud/progress_hud.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:projet/Basededonne/basededonnneClient.dart';
import 'package:projet/Basededonne/basededonnneProduit.dart';
import 'package:projet/Login/loginView.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';

class ListeInventaire extends StatefulWidget {
  @override
  _ListeInventaireState createState() => _ListeInventaireState();
}

List inventaire = List();

class _ListeInventaireState extends State<ListeInventaire> {
  @override
  void initState() {
    super.initState();
    liste();
  }

  liste() async {
    var inserer = DBproduit();
    Future<List> future = inserer.inventaire();
    List inventaire1 = await future;
    setState(() {
      inventaire = inventaire1;
    });
  }

  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async => true,
        child: new Scaffold(
          appBar: AppBar(
             iconTheme: IconThemeData(
                color: Colors.black,
                size: MediaQuery.of(context).size.height * 0.05),
            elevation: 0,backgroundColor: Colors.white,),
          body: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.85,
                margin: EdgeInsets.only(top: 12),
                child: new Column(children: [
                  Container(
                    margin: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.01,
                        top: MediaQuery.of(context).size.height * 0),
                    child: Card(
                      child: Image.asset("assets/icam/camson.JPG",
                          height: MediaQuery.of(context).size.height * 0.1,
                          fit: BoxFit.fitWidth),
                    ),
                  ),
                  Card(
                    elevation: 0,
                    //ito le amban  color: Colors.purpleAccent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0)),
                    color: Colors.pinkAccent[100],
                    child: ListTile(
                      title: Text(
                        "Inventaire",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12.0,
                        ),
                      ),
                      trailing: Icon(Icons.person),
                      //leading: Image.asset('assets/app_sed.jpg'),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.width * 0.01),
                  Container(
                    child: PaginatedDataTable(
                      rowsPerPage: 4,
                      columns: [
                        DataColumn(label: Text('N° Serie')),
                        DataColumn(label: Text('N° QrCode')),
                        DataColumn(label: Text('Designation')),
                      ],
                      source: _DataSource(context),
                    ),
                  ),
                ]),
              ),
            ],
          ),
        ));
  }
}

class _Row {
  _Row(
    this.valueA,
    this.valueB,
    this.valueC,
  );

  final String valueA;
  final String valueB;
  final String valueC;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  _DataSource(this.context) {
    _rows = <_Row>[
      if(inventaire.length!=0)
      for(int i=0;i<=inventaire.length;i++)
      _Row('${inventaire[0]["num_serie"]}', '${inventaire[0]["num_Qrcode"]}', '${inventaire[0]["designation"]}'),
      
    ];
  }

  final BuildContext context;
  List<_Row> _rows;

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow(
      cells: [
        DataCell(Text(row.valueA)),
        DataCell(Text(row.valueB)),
        DataCell(Text(row.valueC)),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
