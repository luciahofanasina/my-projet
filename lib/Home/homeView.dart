import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_hud/progress_hud.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:projet/Client/ClientAdd.dart';
import 'package:projet/Client/ListClients.dart';
import 'package:projet/Login/loginView.dart';
import 'package:projet/Produit/listeProduit.dart';
import 'package:projet/Stock/ListStock.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _passwordVisible = false;
  bool show = false;
  @override
  void initState() {
    super.initState();
  }

  Key key = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async => true,
        child: new Scaffold(
          body: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.85,
                margin: EdgeInsets.only(top: 12),
                child: new Column(children: [
                  Container(
                    margin: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.01,
                        top: MediaQuery.of(context).size.height * 0),
                    child: Card(
                      child: Image.asset("assets/icam/cam.png",
                          height: MediaQuery.of(context).size.height * 0.1,
                          fit: BoxFit.fitWidth),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.135,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              print('scan');
                              Fluttertoast.showToast(
                                msg: "En cours de constuction",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 2,
                              );
                            },
                            child: Card(
                              color: Colors.transparent,
                              elevation: 4.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  ImageIcon(
                                    AssetImage("assets/icam/bar.png"),
                                    size: MediaQuery.of(context).size.height *
                                        0.09,
                                  ),
                                  Text(
                                    'Bar Code',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Color.fromRGBO(152, 203, 222, 1),
                                      fontSize:
                                          MediaQuery.of(context).size.width *
                                              0.03,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 16.0),
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              print('scan');
                              Fluttertoast.showToast(
                                msg: "En cours de constuction",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 2,
                              );
                            },
                            child: Card(
                              color: Colors.transparent,
                              elevation: 4.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  ImageIcon(
                                    AssetImage("assets/icam/qr.png"),
                                    color: Colors.black,
                                    size: MediaQuery.of(context).size.height *
                                        0.09,
                                  ),
                                  Text(
                                    'Qr Code',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Color.fromRGBO(152, 203, 222, 1),
                                      fontSize:
                                          MediaQuery.of(context).size.width *
                                              0.03,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.005,
                        top: MediaQuery.of(context).size.height * 0.02),
                    height: MediaQuery.of(context).size.height * 0.08,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ClientAdd(),
                        ));
                      },
                      textColor: Colors.black,
                      color: Colors.white,
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
                                child: ImageIcon(
                                  AssetImage("assets/2.png"),
                                  color: Colors.white,
                                  size:
                                      MediaQuery.of(context).size.height * 0.05,
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                padding: EdgeInsets.fromLTRB(10, 4, 4, 4),
                                child: Text(
                                  'Nouveau Client',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize:
                                        MediaQuery.of(context).size.width *
                                            0.03,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ),
                  ),
                  
                  Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.005,
                        top: MediaQuery.of(context).size.height * 0.02),
                    height: MediaQuery.of(context).size.height * 0.08,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ListeProduit(),
                        ));
                      },
                      textColor: Colors.black,
                      color: Colors.white,
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
                                child: ImageIcon(
                                  AssetImage("assets/2.png"),
                                  color: Colors.white,
                                  size:
                                      MediaQuery.of(context).size.height * 0.05,
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                padding: EdgeInsets.fromLTRB(10, 4, 4, 4),
                                child: Text(
                                  'Liste des Produits',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize:
                                        MediaQuery.of(context).size.width *
                                            0.03,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.005,
                        top: MediaQuery.of(context).size.height * 0.02),
                    height: MediaQuery.of(context).size.height * 0.08,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ListStock(),
                        ));
                      },
                      textColor: Colors.black,
                      color: Colors.white,
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                       
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
                                child: ImageIcon(
                                  AssetImage("assets/2.png"),
                                  color: Colors.white,
                                  size:
                                      MediaQuery.of(context).size.height * 0.05,
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                padding: EdgeInsets.fromLTRB(10, 4, 4, 4),
                                child: Text(
                                  'Stock',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize:
                                        MediaQuery.of(context).size.width *
                                            0.03,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.005,
                        top: MediaQuery.of(context).size.height * 0.02),
                    height: MediaQuery.of(context).size.height * 0.08,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ListClients(),
                        ));
                      },
                      textColor: Colors.black,
                      color: Colors.white,
                      padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(4, 0, 10, 0),
                                child: ImageIcon(
                                  AssetImage("assets/2.png"),
                                  color: Colors.white,
                                  size:
                                      MediaQuery.of(context).size.height * 0.05,
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                padding: EdgeInsets.fromLTRB(10, 4, 4, 4),
                                child: Text(
                                  'Clients',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize:
                                        MediaQuery.of(context).size.width *
                                            0.03,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          )),
                    ),
                  ),
                ]),
              ),
            ],
          ),
        ));
  }
}
