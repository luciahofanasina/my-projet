import 'dart:convert';
import 'dart:typed_data';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:projet/Basededonne/basededonnneImage.dart';
import 'package:projet/Basededonne/basededonnneProduit.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:typed_data';

class Resultat extends StatefulWidget {
  final String numQrcode;
  Resultat({this.numQrcode});
  @override
  _ResultatState createState() => _ResultatState(numQrcode);
}

class _ResultatState extends State<Resultat> {
  String numQrcode;
  _ResultatState(this.numQrcode);

  List List2 = List();
  var image1;
  Uint8List _bytesImage;
  getOne() async {
    var db = DBproduit();
    var get = db.seule(numQrcode);
    List list1 = await get;

    var bd = DBIMAGE();
    Future<List> getmage = bd.getImage(numQrcode);
    List listim = await getmage;
    setState(() {
      List2 = list1;
      _bytesImage = Base64Decoder().convert(listim[0]['photo_name']);
      image1 = Image.memory(_bytesImage).image;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getOne();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(children: <Widget>[
        Card(
            elevation: 4.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            child: new Image.memory(image1)),
            Text.rich(
                                TextSpan(
                                  style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize:
                                        MediaQuery.of(context).size.width *
                                            0.025,
                                    color: Color.fromRGBO(80, 15, 8, 1),
                                  ),
                                  children: [
                                    
                                    TextSpan(
                                      text: ' ',
                                      style: TextStyle(
                                        color: const Color(0xff959087),
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    TextSpan(
                                      text: '${List2[0]['designation']}\n',
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.width *
                                                0.025,
                                        color: Color.fromRGBO(80, 15, 8, 1),
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    TextSpan(
                                      text: 'Marque: ',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    TextSpan(
                                      text: '${List2[0]['marque']}\n',
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.width *
                                                0.025,
                                        color: const Color(0xd0500f08),
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    TextSpan(
                                      text: 'modele: ',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    TextSpan(
                                      text: '${List2[0]['modele']}\n',
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.width *
                                                0.025,
                                        color: Color.fromRGBO(80, 15, 8, 1),
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    TextSpan(
                                      text: 'quantite: ',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                      ),
                                    ),
                                    TextSpan(
                                      text: '${List2[0]['quantite']}\n',
                                      style: TextStyle(
                                        fontSize:
                                            MediaQuery.of(context).size.width *
                                                0.025,
                                        color: Color.fromRGBO(80, 15, 8, 1),
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                ),
                                textHeightBehavior: TextHeightBehavior(
                                    applyHeightToFirstAscent: false),
                                textAlign: TextAlign.left,
                              ),
                             Container(
                                margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.02,
                                    left: MediaQuery.of(context).size.width *
                                        0.13),
                                height:
                                    MediaQuery.of(context).size.height * 0.08,
                                child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: Text(""),
                                      ),
                                      const SizedBox(width: 16.0),
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            
                                          },
                                          child: Card(
                                            color: Colors.green,
                                            elevation: 4.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0.0)),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Expanded(
                                                  child: Icon(Icons.list),
                                                ),
                                                Expanded(
                                                  child: Text(
                                                    'VOIR lISTE',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.04,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ])),
                           
        // : FileImage(_image[i][5]),
      ]),
    );
  }
}
