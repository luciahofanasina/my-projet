import 'dart:convert';
import 'dart:typed_data';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:projet/Produit/Scan/resultat.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:shared_preferences/shared_preferences.dart';

class Scan extends StatefulWidget {
  @override
  _ScanState createState() => _ScanState();
}

class _ScanState extends State<Scan> {
  String _output = '';
  taille(String _output1) {
    //get string length
    // len = _output1.length;

    setState(() {
      int startIndex = 0;
      String result = _output1.substring(startIndex);
      _output = result;

      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Resultat(numQrcode: _output),
          ));
    });
  }

  @override
  initState() {
    super.initState();
    _scanCode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(children: <Widget>[]),
    );
  }

  void _scanCode() async {
    String cameraScanResult = await scanner.scan();

    setState(() {
      String _output1 = cameraScanResult;
      taille(_output1);
    });
    debugPrint('SCAN RES $cameraScanResult');
  }
}
