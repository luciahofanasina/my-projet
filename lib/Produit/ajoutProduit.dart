import 'dart:io';

import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_hud/progress_hud.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:projet/Basededonne/basededonnneClient.dart';
import 'package:projet/Basededonne/basededonnneImage.dart';
import 'package:projet/Basededonne/basededonnneProduit.dart';
import 'package:projet/Home/homeView.dart';
import 'package:projet/Login/loginView.dart';
import 'package:projet/Produit/listeProduit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:intl/intl.dart';
import 'package:image_utils_class/image_utils_class.dart';

import 'package:image_picker/image_picker.dart';

class ProduitAdd extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        localizationsDelegates: [
          // Here
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          // Here
          const Locale('fr', 'FR'),
        ],
        debugShowCheckedModeBanner: false,
        theme: ThemeData(fontFamily: 'Montserrat'),
        initialRoute: '/add',
        routes: <String, WidgetBuilder>{
          "/add": (BuildContext context) => new ProduitAdd1()
        });
  }
}

class ProduitAdd1 extends StatefulWidget {
  @override
  _ProduitAddState createState() => _ProduitAddState();
}

class _ProduitAddState extends State<ProduitAdd1> {
  bool show = false;
  @override
  void initState() {
    super.initState();
    //image controller
    photos.add(TextEditingController());
    //initir controller
    setState(() {
      designationPortable[0] = Color.fromRGBO(234, 225, 225, 1);
      designationLDC[0] = Color.fromRGBO(234, 225, 225, 1);
      designationUC[0] = Color.fromRGBO(234, 225, 225, 1);

      emplacement1[0] = Color.fromRGBO(234, 225, 225, 1);
      emplacement2[0] = Color.fromRGBO(234, 225, 225, 1);
      emplacement3[0] = Color.fromRGBO(234, 225, 225, 1);
      emplacement4[0] = Color.fromRGBO(234, 225, 225, 1);

      num_serieC.add(new TextEditingController());
      num_QrcodeC.add(new TextEditingController());
      designationC.add(new TextEditingController());
      marqueC.add(new TextEditingController());
      modeleC.add(new TextEditingController());
      produitcodeC.add(new TextEditingController());
      longueurC.add(new TextEditingController());
      largeurC.add(new TextEditingController());
      hauteurC.add(new TextEditingController());
      poidsC.add(new TextEditingController());
      produit_dstatusC.add(new TextEditingController());
      emplacementC.add(new TextEditingController());
      date_entreeC.add(new TextEditingController());
      date_sortieC.add(new TextEditingController());
      info_sortieC.add(new TextEditingController());
      etatC.add(new TextEditingController());
      descriptionC.add(new TextEditingController());
      imagecounter[0] = 3;
    });
  }

  Key key = GlobalKey();
//list controller
  final List<TextEditingController> num_serieC = List();
  final List<TextEditingController> num_QrcodeC = List();
  final List<TextEditingController> designationC = List();
  final List<TextEditingController> marqueC = List();
  final List<TextEditingController> modeleC = List();
  final List<TextEditingController> produitcodeC = List();
  final List<TextEditingController> longueurC = List();
  final List<TextEditingController> largeurC = List();
  final List<TextEditingController> hauteurC = List();
  final List<TextEditingController> poidsC = List();
  final List<TextEditingController> produit_dstatusC = List();
  final List<TextEditingController> emplacementC = List();
  final List<TextEditingController> date_entreeC = List();
  final List<TextEditingController> date_sortieC = List();
  final List<TextEditingController> info_sortieC = List();
  final List<TextEditingController> etatC = List();
  final List<TextEditingController> descriptionC = List();

  final List <TextEditingController> photos = List();
// couleur designation

  Map<int, Color> designationPortable = {};
  Map<int, Color> designationLDC = {};
  Map<int, Color> designationUC = {};

  Map<int, Color> emplacement1 = {};
  Map<int, Color> emplacement2 = {};
  Map<int, Color> emplacement3 = {};
  Map<int, Color> emplacement4 = {};
  //produit status list

  List<String> produitStatusList = <String>[
    'En vente',
    'En stock',
  ];
  int compteur = 1;
  plus() {
    setState(() {
      compteur++;
      num_serieC.add(new TextEditingController());
      num_QrcodeC.add(new TextEditingController());
      designationC.add(new TextEditingController());
      marqueC.add(new TextEditingController());
      modeleC.add(new TextEditingController());
      produitcodeC.add(new TextEditingController());
      longueurC.add(new TextEditingController());
      largeurC.add(new TextEditingController());
      hauteurC.add(new TextEditingController());
      poidsC.add(new TextEditingController());
      produit_dstatusC.add(new TextEditingController());
      emplacementC.add(new TextEditingController());
      date_entreeC.add(new TextEditingController());
      date_sortieC.add(new TextEditingController());
      info_sortieC.add(new TextEditingController());
      etatC.add(new TextEditingController());
      descriptionC.add(new TextEditingController());

      num_serieC[compteur - 1].text = num_serieC[compteur - 2].text;
      num_QrcodeC[compteur - 1].text = num_QrcodeC[compteur - 2].text;
      designationC[compteur - 1].text = designationC[compteur - 2].text;
      marqueC[compteur - 1].text = marqueC[compteur - 2].text;
      modeleC[compteur - 1].text = modeleC[compteur - 2].text;
      produitcodeC[compteur - 1].text = produitcodeC[compteur - 2].text;
      longueurC[compteur - 1].text = longueurC[compteur - 2].text;
      largeurC[compteur - 1].text = largeurC[compteur - 2].text;
      hauteurC[compteur - 1].text = hauteurC[compteur - 2].text;
      poidsC[compteur - 1].text = poidsC[compteur - 2].text;
      produit_dstatusC[compteur - 1].text = produit_dstatusC[compteur - 2].text;
      emplacementC[compteur - 1].text = emplacementC[compteur - 2].text;
      date_entreeC[compteur - 1].text = date_entreeC[compteur - 2].text;
      date_sortieC[compteur - 1].text = date_sortieC[compteur - 2].text;
      info_sortieC[compteur - 1].text = info_sortieC[compteur - 2].text;
      etatC[compteur - 1].text = num_serieC[compteur - 2].text;
      descriptionC[compteur - 1].text = descriptionC[compteur - 2].text;
      designationPortable[compteur - 1] = designationPortable[compteur - 2];
      designationLDC[compteur - 1] = designationLDC[compteur - 2];
      designationUC[compteur - 1] = designationUC[compteur - 2];

      emplacement1[compteur - 1] = emplacement1[compteur - 2];
      emplacement2[compteur - 1] = emplacement2[compteur - 2];
      emplacement3[compteur - 1] = emplacement3[compteur - 2];
      emplacement4[compteur - 1] = emplacement4[compteur - 2];
      imagecounter[compteur - 1] = 3;
     
    photos.add(TextEditingController());
    });
  }

  nouveau() {
    setState(() {
      compteur++;
      num_serieC.add(new TextEditingController());
      num_QrcodeC.add(new TextEditingController());
      designationC.add(new TextEditingController());
      marqueC.add(new TextEditingController());
      modeleC.add(new TextEditingController());
      produitcodeC.add(new TextEditingController());
      longueurC.add(new TextEditingController());
      largeurC.add(new TextEditingController());
      hauteurC.add(new TextEditingController());
      poidsC.add(new TextEditingController());
      produit_dstatusC.add(new TextEditingController());
      emplacementC.add(new TextEditingController());
      date_entreeC.add(new TextEditingController());
      date_sortieC.add(new TextEditingController());
      info_sortieC.add(new TextEditingController());
      etatC.add(new TextEditingController());
      descriptionC.add(new TextEditingController());
      designationPortable[compteur - 1] = Color.fromRGBO(234, 225, 225, 1);
      designationLDC[compteur - 1] = Color.fromRGBO(234, 225, 225, 1);
      designationUC[compteur - 1] = Color.fromRGBO(234, 225, 225, 1);
      emplacement1[compteur - 1] = Color.fromRGBO(234, 225, 225, 1);
      emplacement2[compteur - 1] = Color.fromRGBO(234, 225, 225, 1);
      emplacement3[compteur - 1] = Color.fromRGBO(234, 225, 225, 1);
      emplacement4[compteur - 1] = Color.fromRGBO(234, 225, 225, 1);
      imagecounter[compteur - 1] = 3;
       
    photos.add(TextEditingController());
    });
  }

  //compteur image
  Map<int, int> imagecounter = {};
  incrementImage() {
    setState(() {
      imagecounter[compteur - 1] = imagecounter[compteur - 1] + 1;
    });
  }

//get image
  File _image ;

  Future getCameraImage() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera,
        imageQuality: 50, // <- Reduce Image quality
        maxHeight: 500, // <- reduce the image size
        maxWidth: 500);
    if (image != null) {
      setState(() {
        _image= image;
        photos[compteur - 1].text =
            ImageUtils.fileToBase64(File(image.path));
      });
    }
  }

  //============================== Image from gallery
  Future getGalleryImage() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery,
        imageQuality: 50, // <- Reduce Image quality
        maxHeight: 500, // <- reduce the image size
        maxWidth: 500);
    if (image != null) {
      setState(() {
        _image= image;
        photos[compteur - 1].text =
            ImageUtils.fileToBase64(File(image.path));
      });
    }
  }

  void _onAlertPress() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.camera),
                      title: new Text('camera'),
                      onTap: () {
                        getCameraImage();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Gallery'),
                    onTap: () {
                      getGalleryImage();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  enregistrer() async {
    var bd = DBproduit();
    for (int i = 0; i < compteur; i++) {
      Future<int> add = bd.inserer(
        num_serieC[i].text,
        num_QrcodeC[i].text,
        designationC[i].text,
        marqueC[i].text,
        modeleC[i].text,
        produitcodeC[i].text,
        longueurC[i].text,
        largeurC[i].text,
        hauteurC[i].text,
        poidsC[i].text,
        produit_dstatusC[i].text,
        emplacementC[i].text,
        date_entreeC[i].text,
        date_sortieC[i].text,
        info_sortieC[i].text,
        etatC[i].text,
        descriptionC[i].text,
      );
      int id = await add;
      var im = DBIMAGE();

    
        var image = im.insertImage(photos[i].text, num_QrcodeC[i].text);
      
    }

     CoolAlert.show(
        context: context,
        type: CoolAlertType.success,
        text: "ajout avec succès ",
        confirmBtnColor: Color.fromRGBO(159, 186, 7, 1),
        backgroundColor: Color.fromRGBO(159, 186, 71, 1),
      );

      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ListeProduit(),
          ));
    
  }

  moins() {
    if (compteur >= 0) {
      setState(() {
        compteur--;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async => true,
        child: new Scaffold(
          body: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.85,
                margin: EdgeInsets.only(top: 12),
                child: new Column(children: [
                  Container(
                    margin: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.01,
                        top: MediaQuery.of(context).size.height * 0),
                    child: Card(
                      child: Image.asset("assets/icam/camson.JPG",
                          height: MediaQuery.of(context).size.height * 0.1,
                          fit: BoxFit.fitWidth),
                    ),
                  ),
                  Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0)),
                    color: Colors.pinkAccent[100],
                    child: ListTile(
                      title: Text(
                        "Nouveau Produit",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12.0,
                        ),
                      ),
                      trailing: Icon(Icons.person),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.width * 0.01),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.45,
                    child: SingleChildScrollView(
                      physics: ClampingScrollPhysics(),
                      child: Container(
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.02,
                            right: MediaQuery.of(context).size.width * 0.02),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        child: Column(
                          children: [
                            Container(
                                margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.02,
                                    left: MediaQuery.of(context).size.width *
                                        0.13),
                                height:
                                    MediaQuery.of(context).size.height * 0.08,
                                child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: Text(""),
                                      ),
                                      const SizedBox(width: 16.0),
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            enregistrer();
                                          },
                                          child: Card(
                                            color: Colors.green,
                                            elevation: 4.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0.0)),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Expanded(
                                                  child: Icon(Icons.check),
                                                ),
                                                Expanded(
                                                  child: Text(
                                                    'Valide',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.04,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ])),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.width * 0.01),
                            for (int i = 0; i < compteur; i++)
                              new Column(children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Numero de serie:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          controller: num_serieC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "N° DE",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Numeto QrCode:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          controller: num_QrcodeC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "N° QrCode",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Designation:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          controller: designationC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "Designation",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.08,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              designationC[i].text = "PORTABLE";
                                              designationPortable[i] =
                                                  Color.fromRGBO(
                                                      191, 222, 251, 1);
                                              designationLDC[i] =
                                                  Color.fromRGBO(
                                                      234, 225, 225, 1);
                                              designationUC[i] = Color.fromRGBO(
                                                  234, 225, 225, 1);
                                            });
                                          },
                                          child: Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.08,
                                            child: Card(
                                              color: designationPortable[i],
                                              elevation: 4.0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          0.0)),
                                              child: Text(
                                                "PORTABLE:",
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 16.0),
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              designationC[i].text = "LDC";
                                              designationPortable[i] =
                                                  Color.fromRGBO(
                                                      234, 225, 225, 1);

                                              designationLDC[i] =
                                                  Color.fromRGBO(
                                                      191, 222, 251, 1);
                                              designationUC[i] = Color.fromRGBO(
                                                  234, 225, 225, 1);
                                            });
                                          },
                                          child: Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.08,
                                            child: Card(
                                              color: designationLDC[i],
                                              elevation: 4.0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          0.0)),
                                              child: Text(
                                                "LDC:",
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 16.0),
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              designationC[i].text = "UC";
                                              designationPortable[i] =
                                                  Color.fromRGBO(
                                                      234, 225, 225, 1);

                                              designationLDC[i] =
                                                  Color.fromRGBO(
                                                      234, 225, 225, 1);
                                              designationUC[i] = Color.fromRGBO(
                                                  191, 222, 251, 1);
                                            });
                                          },
                                          child: Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.08,
                                            child: Card(
                                              color: designationUC[i],
                                              elevation: 4.0,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          0.0)),
                                              child: Text(
                                                "UC:",
                                                textAlign: TextAlign.left,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Modèle:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          controller: modeleC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "Modèle",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Produit code:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          controller: produitcodeC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "Produit code",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.08,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: SizedBox(
                                          width: 15,
                                          child: Text(
                                            "Dimension:",
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 3.0),
                                      Expanded(
                                        flex: 1,
                                        child: FocusScope(
                                            child: Focus(
                                          onFocusChange: (focus) =>
                                              print("focus: $focus"),
                                          child: TextField(
                                            controller: longueurC[i],
                                            autocorrect: true,
                                            decoration: InputDecoration(
                                              contentPadding: EdgeInsets.all(8),
                                              fillColor: Colors.grey[300],
                                              filled: true,
                                              hintText: "longueur",
                                              hintStyle: TextStyle(
                                                color: Colors.black,
                                              ),
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                borderSide: BorderSide(
                                                  color:
                                                      Colors.deepPurpleAccent,
                                                  width: 3,
                                                ),
                                              ),
                                              disabledBorder:
                                                  OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey[300]),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey[300]),
                                              ),
                                            ),
                                          ),
                                        )),
                                      ),
                                      const SizedBox(width: 3.0),
                                      Expanded(
                                        flex: 1,
                                        child: FocusScope(
                                            child: Focus(
                                          onFocusChange: (focus) =>
                                              print("focus: $focus"),
                                          child: TextField(
                                            controller: largeurC[i],
                                            autocorrect: true,
                                            decoration: InputDecoration(
                                              contentPadding: EdgeInsets.all(8),
                                              fillColor: Colors.grey[300],
                                              filled: true,
                                              hintText: "largeur",
                                              hintStyle: TextStyle(
                                                color: Colors.black,
                                              ),
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                borderSide: BorderSide(
                                                  color:
                                                      Colors.deepPurpleAccent,
                                                  width: 3,
                                                ),
                                              ),
                                              disabledBorder:
                                                  OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey[300]),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey[300]),
                                              ),
                                            ),
                                          ),
                                        )),
                                      ),
                                      const SizedBox(width: 3.0),
                                      Expanded(
                                        flex: 1,
                                        child: FocusScope(
                                            child: Focus(
                                          onFocusChange: (focus) =>
                                              print("focus: $focus"),
                                          child: TextField(
                                            controller: hauteurC[i],
                                            autocorrect: true,
                                            decoration: InputDecoration(
                                              contentPadding: EdgeInsets.all(8),
                                              fillColor: Colors.grey[300],
                                              filled: true,
                                              hintText: "hauteur",
                                              hintStyle: TextStyle(
                                                color: Colors.black,
                                              ),
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                                borderSide: BorderSide(
                                                  color:
                                                      Colors.deepPurpleAccent,
                                                  width: 3,
                                                ),
                                              ),
                                              disabledBorder:
                                                  OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey[300]),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    width: 1,
                                                    color: Colors.grey[300]),
                                              ),
                                            ),
                                          ),
                                        )),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Poids:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          controller: poidsC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "Poids",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Logistique:",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(fontSize: 22),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(child: Text('')),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Marque:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    /*  Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: DropdownButton(
                                          underline:
                                                              Container(),
                                          items: produitStatusList
                                              .map((value) =>
                                                  new DropdownMenuItem(
                                                    child: new Text(
                                                      value,
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 15,
                                                      ),
                                                    ),
                                                    value: value,
                                                  ))
                                              .toList(),
                                          onChanged: (secteurType) {
                                            setState(() {
                                              produit_dstatusC[i].text =
                                                  secteurType;

                                              FocusScope.of(context)
                                                  .requestFocus(
                                                      new FocusNode());
                                            });
                                          },
                                          value: produit_dstatusC[i].text ,
                                          dropdownColor:
                                            Colors.grey[300],
                                          iconEnabledColor: Colors.red,
                                          iconDisabledColor: Colors.black,
                                          isExpanded: true,
                                          hint: Text(
                                            '                        ',
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                        ),
                                      )),
                                    ), */
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Emplacement:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          controller: emplacementC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.08,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              emplacementC[i].text = "MAGASIN";
                                            });
                                          },
                                          child: Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.08,
                                            decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    234, 225, 225, 1),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10))),
                                            child: Icon(
                                              Icons.shopping_cart,
                                              color: Colors.black,
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 5.0),
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              emplacementC[i].text = "STOCK";
                                            });
                                          },
                                          child: Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.08,
                                            decoration: BoxDecoration(
                                                color: Color.fromRGBO(
                                                    234, 225, 225, 1),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10))),
                                            child: Image.asset(
                                              'assets/icam/panier.png',
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 5.0),
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              emplacementC[i].text =
                                                  "REPARATION";
                                            });
                                          },
                                          child: Card(
                                            color: designationUC[i],
                                            elevation: 4.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0.0)),
                                            child: Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.08,
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      234, 225, 225, 1),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(10))),
                                              child: Icon(
                                                Icons.computer,
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 5.0),
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              emplacementC[i].text =
                                                  "RECYCLAGE";
                                            });
                                          },
                                          child: Card(
                                            color: designationUC[i],
                                            elevation: 4.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0.0)),
                                            child: Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.08,
                                              decoration: BoxDecoration(
                                                  color: Color.fromRGBO(
                                                      234, 225, 225, 1),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(10))),
                                              child: Image.asset(
                                                'assets/icam/recycle.png',
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                               
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Date d'entree:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextFormField(
                                          controller: date_entreeC[i],
                                          decoration: InputDecoration(
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: '',
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                color: Colors.grey[300],
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                width: 1,
                                                color: Colors.grey[300],
                                              ),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                width: 1,
                                                color: Colors.grey[300],
                                              ),
                                            ),
                                          ),
                                          onTap: () async {
                                            DateTime date = DateTime(1900);

                                            FocusScope.of(context)
                                                .requestFocus(new FocusNode());

                                            date = await showDatePicker(
                                                builder: (BuildContext context,
                                                    Widget child) {
                                                  return Theme(
                                                    data: ThemeData(
                                                      primarySwatch:
                                                          Colors.grey,
                                                      splashColor: Colors.black,
                                                      textTheme: TextTheme(
                                                        subtitle1: TextStyle(
                                                            color:
                                                                Colors.black),
                                                        button: TextStyle(
                                                            color:
                                                                Colors.black),
                                                      ),
                                                      accentColor: Colors.black,
                                                      colorScheme:
                                                          ColorScheme.light(
                                                              primary: Colors
                                                                      .pinkAccent[
                                                                  100],
                                                              primaryVariant:
                                                                  Colors.black,
                                                              secondaryVariant:
                                                                  Colors.black,
                                                              onSecondary:
                                                                  Colors.black,
                                                              onPrimary:
                                                                  Colors.white,
                                                              surface:
                                                                  Colors.black,
                                                              onSurface:
                                                                  Colors.black,
                                                              secondary:
                                                                  Colors.black),
                                                      dialogBackgroundColor:
                                                          Colors.white,
                                                    ),
                                                    child: child,
                                                  );
                                                },
                                                context: context,
                                                locale: Locale('fr'),
                                                initialDate: DateTime.now(),
                                                firstDate: DateTime(1900),
                                                lastDate: DateTime(2100));

                                            FocusScope.of(context)
                                                .requestFocus(new FocusNode());
                                            setState(() {
                                              String dateNaiss1 =
                                                  date.toString();

                                              DateTime dateTime =
                                                  DateTime.parse(dateNaiss1);
                                              String dateformat =
                                                  DateFormat("dd/MM/yyyy")
                                                      .format(dateTime);

                                              date_entreeC[i].text = dateformat;
                                            });
                                          },
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Date de sortie:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextFormField(
                                          controller: date_sortieC[i],
                                          decoration: InputDecoration(
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: '',
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                color: Colors.grey[300],
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                width: 1,
                                                color: Colors.grey[300],
                                              ),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                width: 1,
                                                color: Colors.grey[300],
                                              ),
                                            ),
                                          ),
                                          onTap: () async {
                                            DateTime date = DateTime(1900);

                                            FocusScope.of(context)
                                                .requestFocus(new FocusNode());

                                            date = await showDatePicker(
                                                builder: (BuildContext context,
                                                    Widget child) {
                                                  return Theme(
                                                    data: ThemeData(
                                                      primarySwatch:
                                                          Colors.grey,
                                                      splashColor: Colors.black,
                                                      textTheme: TextTheme(
                                                        subtitle1: TextStyle(
                                                            color:
                                                                Colors.black),
                                                        button: TextStyle(
                                                            color:
                                                                Colors.black),
                                                      ),
                                                      accentColor: Colors.black,
                                                      colorScheme:
                                                          ColorScheme.light(
                                                              primary: Colors
                                                                      .pinkAccent[
                                                                  100],
                                                              primaryVariant:
                                                                  Colors.black,
                                                              secondaryVariant:
                                                                  Colors.black,
                                                              onSecondary:
                                                                  Colors.black,
                                                              onPrimary:
                                                                  Colors.white,
                                                              surface:
                                                                  Colors.black,
                                                              onSurface:
                                                                  Colors.black,
                                                              secondary:
                                                                  Colors.black),
                                                      dialogBackgroundColor:
                                                          Colors.white,
                                                    ),
                                                    child: child,
                                                  );
                                                },
                                                context: context,
                                                locale: Locale('fr'),
                                                initialDate: DateTime.now(),
                                                firstDate: DateTime(1900),
                                                lastDate: DateTime(2100));

                                            FocusScope.of(context)
                                                .requestFocus(new FocusNode());
                                            setState(() {
                                              String dateNaiss1 =
                                                  date.toString();

                                              DateTime dateTime =
                                                  DateTime.parse(dateNaiss1);
                                              String dateformat =
                                                  DateFormat("dd/MM/yyyy")
                                                      .format(dateTime);

                                              date_sortieC[i].text = dateformat;
                                            });
                                          },
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Info sortie:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          controller: info_sortieC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Etat:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          controller: etatC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.01),
                                Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.08,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              etatC[i].text = "Neuf";
                                            });
                                          },
                                          child: Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.08,
                                              decoration: BoxDecoration(
                                                  color: Colors.green,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(10))),
                                              child: Text('A')),
                                        ),
                                      ),
                                      const SizedBox(width: 5.0),
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              etatC[i].text = "Occasion";
                                            });
                                          },
                                          child: Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.08,
                                            decoration: BoxDecoration(
                                                color: Colors.yellow,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10))),
                                            child: Text("B"),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 5.0),
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              etatC[i].text = "Hors service";
                                            });
                                          },
                                          child: Card(
                                            color: designationUC[i],
                                            elevation: 4.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0.0)),
                                            child: Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.08,
                                                decoration: BoxDecoration(
                                                    color: Colors.red,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                                child: Text('C')),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(width: 5.0),
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              etatC[i].text = "Reconditionné";
                                            });
                                          },
                                          child: Card(
                                            color: designationUC[i],
                                            elevation: 4.0,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0.0)),
                                            child: Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.08,
                                                decoration: BoxDecoration(
                                                    color: Colors.lightGreen,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                                child: Text('D')),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Description:",
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(
                                      child: FocusScope(
                                          child: Focus(
                                        onFocusChange: (focus) =>
                                            print("focus: $focus"),
                                        child: TextField(
                                          maxLines: 10,
                                          controller: descriptionC[i],
                                          autocorrect: true,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(8),
                                            fillColor: Colors.grey[300],
                                            filled: true,
                                            hintText: "",
                                            hintStyle: TextStyle(
                                              color: Colors.black,
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              borderSide: BorderSide(
                                                color: Colors.deepPurpleAccent,
                                                width: 3,
                                              ),
                                            ),
                                            disabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1,
                                                  color: Colors.grey[300]),
                                            ),
                                          ),
                                        ),
                                      )),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: 25,
                                        child: Text(
                                          "Upload image:",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(fontSize: 15),
                                        ),
                                      ),
                                    ),
                                    const SizedBox(width: 16.0),
                                    Expanded(child: Text('')),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.02),
                                  height:
                                      MediaQuery.of(context).size.height * 0.10,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                          child: InkWell(
                                        onTap: () {},
                                        child: Card(
                                          elevation: 4.0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          child:  _image== null
                                              ? Image.asset(
                                                  "assets/icam/image.png")
                                              : FileImage(_image),
                                        ),
                                      )),
                                      const SizedBox(width: 16.0),
                                      Expanded(
                                          child: InkWell(
                                        onTap: () {},
                                        child: Card(
                                          elevation: 4.0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          child:Image.asset(
                                                  "assets/icam/image.png")
                                              ,
                                        ),
                                      )),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.02),
                                  height:
                                      MediaQuery.of(context).size.height * 0.10,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                          child: InkWell(
                                        onTap: () {},
                                        child: Card(
                                          elevation: 4.0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          child:Image.asset(
                                                  "assets/icam/image.png")
                                              ,
                                        ),
                                      )),
                                      const SizedBox(width: 16.0),
                                      Expanded(
                                          child: InkWell(
                                        onTap: () {},
                                        child: Card(
                                          elevation: 4.0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          child: Image.asset(
                                                  "assets/icam/image.png")
                                              ,
                                        ),
                                      )),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.02),
                                  height:
                                      MediaQuery.of(context).size.height * 0.10,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                          child: InkWell(
                                        onTap: () {},
                                        child: Card(
                                          elevation: 4.0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          child: Image.asset(
                                                  "assets/icam/image.png")
                                              ,
                                        ),
                                      )),
                                      const SizedBox(width: 16.0),
                                      Expanded(
                                          child: InkWell(
                                        onTap: () {},
                                        child: Card(
                                          elevation: 4.0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          child: Image.asset(
                                                  "assets/icam/image.png")
                                             ,
                                        ),
                                      )),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.width *
                                        0.01),
                                Container(
                                  margin: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.02),
                                  height:
                                      MediaQuery.of(context).size.height * 0.10,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                          child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            incrementImage();
                                          });
                                        },
                                        child: Card(
                                          elevation: 4.0,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          child: Image.asset(
                                              "assets/icam/image.png"),
                                        ),
                                      )),
                                      const SizedBox(width: 16.0),
                                      Expanded(
                                          child: RaisedButton(
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                            Radius.circular(0.0),
                                          )),
                                          child: Text(
                                            'Ajouter plus de photo',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12.0),
                                          ),
                                          color: Colors.grey,
                                          splashColor: Colors.blue,
                                          onPressed: () { _onAlertPress();},
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ]),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    height: 40.0,
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.3,
                        right: MediaQuery.of(context).size.width * 0.2),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                        Radius.circular(0.0),
                      )),
                      child: Text(
                        'Ajouter un produit similaire',
                        style: TextStyle(color: Colors.white, fontSize: 12.0),
                      ),
                      color: Colors.grey,
                      splashColor: Colors.blue,
                      onPressed: () {},
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.02,
                          left: MediaQuery.of(context).size.width * 0.13),
                      height: MediaQuery.of(context).size.height * 0.08,
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  nouveau();
                                },
                                child: Card(
                                  color: Colors.green,
                                  elevation: 4.0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'V.Produit',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.04,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16.0),
                            Expanded(
                              child: FloatingActionButton(
                                backgroundColor: Colors.purpleAccent,
                                child: Icon(Icons.add),
                                onPressed: () {
                                  setState(() {
                                    plus();
                                  });
                                },
                              ),
                            ),
                          ]))
                ]),
              ),
            ],
          ),
        ));
  }
}
