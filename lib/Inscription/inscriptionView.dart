import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_hud/progress_hud.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:projet/Login/loginView.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';

class NewAccount extends StatefulWidget {
  @override
  _NewAccountState createState() => _NewAccountState();
}

class _NewAccountState extends State<NewAccount> {
  bool _passwordVisible = false;
  bool show = false;
  @override
  void initState() {
    super.initState();
  }

  Key key = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async => true,
        child: new Scaffold(
          body: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.85,
                margin: EdgeInsets.only(top: 12),
                child: new Column(children: [
                  Container(
                    margin: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.01,
                        top: MediaQuery.of(context).size.height * 0),
                    child: Card(
                      child: Image.asset("assets/icam/camson.JPG",
                          height: MediaQuery.of(context).size.height * 0.1,
                          fit: BoxFit.fitWidth),
                    ),
                  ),
                  Card(
                    elevation: 0,
                    //ito le amban  color: Colors.purpleAccent,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0)),
                    color: Colors.pinkAccent[100],
                    child: ListTile(
                      title: Text(
                        "Créer un compte",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12.0,
                        ),
                      ),
                     
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.width * 0.01),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.4,
                    child: SingleChildScrollView(
                      physics: ClampingScrollPhysics(),
                      child: Container(
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.02,
                            right: MediaQuery.of(context).size.width * 0.02),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        child: Form(
                          key: key,
                          child: Column(
                            children: <Widget>[
                              Text(
                                "Nom:",
                                textAlign: TextAlign.left,
                              ),
                              FocusScope(
                                  child: Focus(
                                onFocusChange: (focus) =>
                                    print("focus: $focus"),
                                child: TextField(
                                  // controller: _matricule,
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    fillColor: Colors.grey[300],
                                    filled: true,
                                    hintText: "",
                                    hintStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide(
                                        color: Colors.deepPurpleAccent,
                                        width: 3,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                  ),
                                ),
                              )),
                              Text(
                                "Prénom:",
                                textAlign: TextAlign.left,
                              ),
                              FocusScope(
                                  child: Focus(
                                onFocusChange: (focus) =>
                                    print("focus: $focus"),
                                child: TextField(
                                  // controller: _matricule,
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    fillColor: Colors.grey[300],
                                    filled: true,
                                    hintText: "",
                                    hintStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide(
                                        color: Colors.deepPurpleAccent,
                                        width: 3,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                  ),
                                ),
                              )),
                              Text(
                                "Téléphone:",
                                textAlign: TextAlign.left,
                              ),
                              FocusScope(
                                  child: Focus(
                                onFocusChange: (focus) =>
                                    print("focus: $focus"),
                                child: TextField(
                                  // controller: _matricule,
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    fillColor: Colors.grey[300],
                                    filled: true,
                                    hintText: "",
                                    hintStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide(
                                        color: Colors.deepPurpleAccent,
                                        width: 3,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                  ),
                                ),
                              )),
                              Text(
                                "Adresse mail:",
                                textAlign: TextAlign.left,
                              ),
                              FocusScope(
                                  child: Focus(
                                onFocusChange: (focus) =>
                                    print("focus: $focus"),
                                child: TextField(
                                  // controller: _matricule,
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    fillColor: Colors.grey[300],
                                    filled: true,
                                    hintText: "",
                                    hintStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide(
                                        color: Colors.deepPurpleAccent,
                                        width: 3,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                  ),
                                ),
                              )),
                              Text(
                                "Mot de passe:",
                                textAlign: TextAlign.left,
                              ),
                              FocusScope(
                                  child: Focus(
                                onFocusChange: (focus) =>
                                    print("focus: $focus"),
                                child: TextField(
                                  // controller: _matricule,
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    fillColor: Colors.grey[300],
                                    filled: true,
                                    hintText: "",
                                    hintStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide(
                                        color: Colors.deepPurpleAccent,
                                        width: 3,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                  ),
                                ),
                              )),
                              Text(
                                "Confirmer Mot de passe:",
                                textAlign: TextAlign.left,
                              ),
                              FocusScope(
                                  child: Focus(
                                onFocusChange: (focus) =>
                                    print("focus: $focus"),
                                child: TextField(
                                  // controller: _matricule,
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(8),
                                    fillColor: Colors.grey[300],
                                    filled: true,
                                    hintText: "",
                                    hintStyle: TextStyle(
                                      color: Colors.black,
                                    ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide(
                                        color: Colors.deepPurpleAccent,
                                        width: 3,
                                      ),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          width: 1, color: Colors.grey[300]),
                                    ),
                                  ),
                                ),
                              )),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin:EdgeInsets.only(top:MediaQuery.of(context).size.height * 0.1),
                      height: MediaQuery.of(context).size.height * 0.1,
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => Login(),
                                  ));
                                },
                                child: Card(
                                  color: Colors.black,
                                  elevation: 4.0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'ANNULER',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16.0),
                            Expanded(
                              child: InkWell(
                                onTap: () async {},
                                child: Card(
                                  color: Colors.purpleAccent,
                                  elevation: 4.0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'ENREGISTRER',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ]))
                ]),
              ),
            ],
          ),
        ));
  }
}
