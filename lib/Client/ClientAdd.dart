import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_hud/progress_hud.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:projet/Basededonne/basededonnneClient.dart';
import 'package:projet/Client/ListClients.dart';
import 'package:projet/Home/homeView.dart';
import 'package:projet/Login/loginView.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:intl/intl.dart';

class ClientAdd extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        localizationsDelegates: [
          // Here
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          // Here
          const Locale('fr', 'FR'),
        ],
        debugShowCheckedModeBanner: false,
        theme: ThemeData(fontFamily: 'Montserrat'),
        initialRoute: '/inscription',
        routes: <String, WidgetBuilder>{
          "/inscription": (BuildContext context) => new ClientAdd1()
        });
  }
}

class ClientAdd1 extends StatefulWidget {
  @override
  _ClientAddState createState() => _ClientAddState();
}

class _ClientAddState extends State<ClientAdd1> {
  bool _passwordVisible = false;
  bool show = false;
  @override
  void initState() {
    super.initState();
    for(int a=0;a<=2;a++)
    controller.add(TextEditingController());
  }

  Key key = GlobalKey();

  ajoutClient() async {
    var inserer = Client();
    var cli = inserer.insert(controller[0].text, controller[1].text,
        controller[2].text);
    Fluttertoast.showToast(
        msg: 'Ajout avec succés', toastLength: Toast.LENGTH_SHORT);
        
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => ListClients()));
  }

  List<TextEditingController> controller = List();
  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async => true,
        child: new Scaffold(
          
          body: Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.85,
                margin: EdgeInsets.only(top: 12),
                child: new Column(children: [
                  Container(
                    margin: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height * 0.01,
                        top: MediaQuery.of(context).size.height * 0),
                    child: Card(
                      child: Image.asset("assets/icam/camson.JPG",
                          height: MediaQuery.of(context).size.height * 0.1,
                          fit: BoxFit.fitWidth),
                    ),
                  ),
                  Card(
                    elevation: 0,
                     shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0)),
                    color: Colors.pinkAccent[100],
                    child: ListTile(
                      title: Text(
                        "Ajouter un nouveau Client",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 12.0,
                        ),
                      ),
                       trailing: Icon(Icons.person),
                     
                     ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.width * 0.01),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: SizedBox(
                          width: 25,
                          child: Text(
                            "Société  :",
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        child: FocusScope(
                            child: Focus(
                          onFocusChange: (focus) => print("focus: $focus"),
                          child: TextField(
                            controller: controller[0],
                            autocorrect: true,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(8),
                              fillColor: Colors.grey[300],
                              filled: true,
                              hintText: "Nom de la société",
                              hintStyle: TextStyle(
                                color: Colors.black,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(
                                  color: Colors.deepPurpleAccent,
                                  width: 3,
                                ),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                    width: 1, color: Colors.grey[300]),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                    width: 1, color: Colors.grey[300]),
                              ),
                            ),
                          ),
                        )),
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.width * 0.01),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: SizedBox(
                          width: 25,
                          child: Text(
                            "N° Contrat :",
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        child: FocusScope(
                            child: Focus(
                          onFocusChange: (focus) => print("focus: $focus"),
                          child: TextField(
                            controller: controller[1],
                            autocorrect: true,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(8),
                              fillColor: Colors.grey[300],
                              filled: true,
                              hintText: "Numero de la contrat",
                              hintStyle: TextStyle(
                                color: Colors.black,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(
                                  color: Colors.deepPurpleAccent,
                                  width: 3,
                                ),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                    width: 1, color: Colors.grey[300]),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                    width: 1, color: Colors.grey[300]),
                              ),
                            ),
                          ),
                        )),
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.width * 0.01),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: SizedBox(
                          width: 25,
                          child: Text(
                            "Date :",
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      const SizedBox(width: 16.0),
                      Expanded(
                        child: FocusScope(
                            child: Focus(
                          onFocusChange: (focus) => print("focus: $focus"),
                          child: TextField(
                            onTap: () async {
                              DateTime date = DateTime(1900);
                              date = await showDatePicker(
                                  builder:
                                      (BuildContext context, Widget child) {
                                    return Theme(
                                      data: ThemeData(
                                        primarySwatch: Colors.grey,
                                        splashColor: Colors.black,
                                        textTheme: TextTheme(
                                          subtitle1:
                                              TextStyle(color: Colors.black),
                                          button:
                                              TextStyle(color: Colors.black),
                                        ),
                                        accentColor: Colors.black,
                                        colorScheme: ColorScheme.light(
                                            primary:
                                               Colors.pinkAccent[100],
                                            primaryVariant: Colors.black,
                                            secondaryVariant: Colors.black,
                                            onSecondary: Colors.black,
                                            onPrimary: Colors.white,
                                            surface: Colors.black,
                                            onSurface: Colors.black,
                                            secondary: Colors.black),
                                        dialogBackgroundColor: Colors.white,
                                      ),
                                      child: child,
                                    );
                                  },
                                  context: context,
                                  locale: Locale('fr'),
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(1900),
                                  lastDate: DateTime(2100));

                              String dateNaiss1 = date.toString();

                              DateTime dateTime = DateTime.parse(dateNaiss1);
                              String dateformat =
                                  DateFormat("dd/MM/yyyy").format(dateTime);
                              setState(() {
                                controller[2].text = dateformat;
                              });
                            },
                            controller: controller[2],
                            autocorrect: true,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(8),
                              fillColor: Colors.grey[300],
                              filled: true,
                              hintText: "",
                              hintStyle: TextStyle(
                                color: Colors.black,
                              ),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(
                                  color: Colors.deepPurpleAccent,
                                  width: 3,
                                ),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                    width: 1, color: Colors.grey[300]),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: BorderSide(
                                    width: 1, color: Colors.grey[300]),
                              ),
                            ),
                          ),
                        )),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.1),
                      height: MediaQuery.of(context).size.height * 0.1,
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => Home(),
                                  ));
                                },
                                child: Card(
                                  color: Colors.black,
                                  elevation: 4.0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'ANNULER',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16.0),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  ajoutClient();
                                },
                                child: Card(
                                  color: Colors.purpleAccent,
                                  elevation: 4.0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'ENREGISTRER',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ]))
                ]),
              ),
            ],
          ),
        ));
  }
}
